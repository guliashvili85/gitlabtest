package com.example.gitlabdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitLabDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitLabDemoApplication.class, args);
    }

}
